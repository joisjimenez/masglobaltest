﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MASGlobalTest.BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO.Tests
{
    [TestClass()]
    public class ContractTests
    {
        [TestMethod(), TestCategory("Business Logic")]
        public void GetAnnualSalaryByHourlyContractTest()
        {
            HourlyContract hourlyContract = new HourlyContract(60000);
            decimal salary = hourlyContract.GetAnnualSalary();

            // expected
            var hourlyTestSalary = 120 * hourlyContract.Salary * 12;

            Assert.AreEqual(salary, hourlyTestSalary);

        }

        [TestMethod(), TestCategory("Business Logic")]
        public void GetAnnualSalaryByMonthlyContractTest()
        {
            MonthlyContract monthlyContract = new MonthlyContract(80000);
            decimal salary = monthlyContract.GetAnnualSalary();

            // expected
            var hourlyTestSalary = monthlyContract.Salary * 12;

            Assert.AreEqual(salary, hourlyTestSalary);

        }
    }
}