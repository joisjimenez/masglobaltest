﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO
{
    /// <summary>
    /// Contract Abstract Class
    /// </summary>
    public abstract class ContractAbstract
    {
        /// <summary>
        /// Defines the contract type
        /// </summary>
        public Enums.EnumContractType ContractType { get; internal set; }

        /// <summary>
        /// Contract Salary
        /// </summary>
        public decimal Salary { get; protected set; }

        /// <summary>
        /// Calculates the contract salary
        /// </summary>
        /// <returns>Contract Salary</returns>
        public abstract decimal GetAnnualSalary();

        public ContractAbstract(decimal salary)
        {

        }
    }
}
