﻿using MASGlobalTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO
{
    /// <summary>
    /// Employee Class
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Employee ID
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Employee Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Employee Contract Type
        /// </summary>
        public string ContractTypeName { get; private set; }

        /// <summary>
        /// Employee Role ID
        /// </summary>
        public int RoleId { get; private set; }

        /// <summary>
        /// Employee Role Name
        /// </summary>
        public string RoleName { get; private set; }

        /// <summary>
        /// Employee Role Description
        /// </summary>
        public string RoleDescription { get; private set; } = null;

        /// <summary>
        /// Employee Hourly Contract Salary
        /// </summary>
        public decimal HourlySalary { get; private set; }

        /// <summary>
        /// Employee Monthly Contract Salary
        /// </summary>
        public decimal MonthlySalary { get; private set; }

        /// <summary>
        /// Contract Type Object
        /// </summary>
        public ContractAbstract ContractType { get; private set; }

        /// <summary>
        /// Initializes a new Employee class
        /// </summary>
        /// <param name="id">Employee ID</param>
        /// <param name="name">Employee Name</param>
        /// <param name="contractTypeName">Employee Contract Type</param>
        /// <param name="roleId">Employee Role ID</param>
        /// <param name="roleName">Employee Role Name</param>
        /// <param name="roleDescription">Employee Role Description</param>
        /// <param name="hourlySalary">Employee Hourly Contract Salary</param>
        /// <param name="monthlySalary">Employee Monthly Contract Salary</param>
        public Employee(int id, string name, string contractTypeName, int roleId, string roleName, string roleDescription, decimal hourlySalary, decimal monthlySalary)
        {
            Id = id;
            Name = name;
            RoleId = roleId;
            RoleName = roleName;
            RoleDescription = roleDescription;
            HourlySalary = hourlySalary;
            MonthlySalary = monthlySalary;
            ContractTypeName = contractTypeName;

            switch (contractTypeName.ToLower())
            {
                case "monthlysalaryemployee":
                    ContractType = new MonthlyContract(monthlySalary);
                    break;
                case "hourlysalaryemployee":
                    ContractType = new HourlyContract(hourlySalary);
                    break;
            }
        }

        public static List<Employee> GetEmployees()
        {
            List<Employee> listEmployees = new List<Employee>();
            try
            {
                // Get employees data
                string data = EmployeeData.GetEmployees();

                if (string.IsNullOrEmpty(data))
                    throw new NullReferenceException("No data found");

                // deserialize result
                listEmployees = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employee>>(data);

            }
            catch (Exception ex)
            {

                throw;
            }
            return listEmployees;
        }

        public static Employee GetEmployeeByID(int id)
        {
            Employee employee = null;
            try
            {
                // Get employees data
                string data = EmployeeData.GetEmployees();

                if (string.IsNullOrEmpty(data))
                    throw new NullReferenceException("No employees data found");

                // deserialize result
                var listEmployees = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employee>>(data);

                if (listEmployees.Any())
                {
                    // filter by ID
                    employee = listEmployees.FirstOrDefault(x => x.Id == id);

                    if (employee == null)
                        throw new KeyNotFoundException("Employee not found");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return employee;
        }
    }
}
