﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO
{
    public class HourlyContract : ContractAbstract
    {
        public HourlyContract(decimal salary) : base(salary)
        {
            ContractType = Enums.EnumContractType.HourlySalaryEmployee;
            Salary = salary;
        }

        public override decimal GetAnnualSalary()
        {
            decimal annualSalary = 120 * Salary * 12;
            return annualSalary;
        }
    }
}
