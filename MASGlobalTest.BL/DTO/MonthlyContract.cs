﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO
{
    public class MonthlyContract : ContractAbstract
    {
        public MonthlyContract(decimal salary) : base(salary)
        {
            ContractType = Enums.EnumContractType.MonthlySalaryEmployee;
            Salary = salary;
        }

        public override decimal GetAnnualSalary()
        {
            decimal annualSalary = Salary * 12;
            return annualSalary;
        }
    }
}
