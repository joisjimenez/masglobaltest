﻿using MASGlobalTest.BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MASGlobalTest.UI.Controllers
{
    public class SalaryController : Controller
    {
        // GET: Salary
        public ActionResult Index(string id)
        {
            EmployeeController employeeAPI = new EmployeeController();
            var employees = employeeAPI.Get();

            if (!string.IsNullOrEmpty(id))
            {
                int idEmployee;
                if (Int32.TryParse(id, out idEmployee))
                {
                    employees = employees.Where(x => x.Id == idEmployee).ToList();
                }
            }

            return View(employees);
        }

        // GET: Salary/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


    }
}
