﻿using MASGlobalTest.BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MASGlobalTest.UI.Controllers
{
    public class EmployeeController : ApiController
    {
        // GET: api/Employee
        public IEnumerable<Employee> Get()
        {
            try
            {
                return Employee.GetEmployees();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
                return null;
            }
        }

        // GET: api/Employee/5
        public Employee Get(int id)
        {
            try
            {
                return Employee.GetEmployeeByID(id);
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
                return null;
            }
        }


    }
}
