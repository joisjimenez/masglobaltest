﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MASGlobalTest.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.Data.Tests
{
    [TestClass()]
    public class EmployeeDataTests
    {
        [TestMethod(), TestCategory("Data")]
        public void GetEmployeesDataTest()
        {
            string data = EmployeeData.GetEmployees();

            Assert.IsTrue(!string.IsNullOrEmpty(data));
        }      
    }
}