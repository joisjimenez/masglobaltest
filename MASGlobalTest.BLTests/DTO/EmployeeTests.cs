﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MASGlobalTest.BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASGlobalTest.BL.DTO.Tests
{
    [TestClass()]
    public class EmployeeTests
    {

        [TestMethod(), TestCategory("Business Logic")]
        public void GetEmployeesTest()
        {
            List<Employee> listEmployees = Employee.GetEmployees();

            Assert.IsTrue(listEmployees != null);
        }

        [TestMethod(), TestCategory("Business Logic")]
        public void GetEmployeeByIDTest()
        {
            Employee employee = Employee.GetEmployeeByID(1);

            decimal annualSalary = employee.ContractType.GetAnnualSalary();

            Assert.IsTrue(employee != null);
        }
    }
}