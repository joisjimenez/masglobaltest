﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace MASGlobalTest.Data
{
    /// <summary>
    /// Employee Data Class
    /// </summary>
    public class EmployeeData
    {
        /// <summary>
        /// Get a list of employees from API.
        /// </summary>
        /// <returns>Raw employees as string</returns>
        public static string GetEmployees()
        {
            string data = string.Empty;
            try
            {
                var client = new RestClient("http://masglobaltestapi.azurewebsites.net/api/");
                var request = new RestRequest("Employees", Method.GET);
                IRestResponse response = client.Execute(request);
                data = response.Content;

            }
            catch (Exception ex)
            {

                throw;
            }

            return data;
        }
    }
}
